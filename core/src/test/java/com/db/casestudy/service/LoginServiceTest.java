package com.db.casestudy.service;

import com.db.casestudy.handler.UserHandler;
import com.db.casestudy.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class LoginServiceTest {

    @InjectMocks
    LoginService loginService;

    @Mock
    UserHandler userHandler;

    ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = new ObjectMapper();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void successLogin(){
        String name = "name";
        String pwd = "pwd";
        User userFromDB = new User(name, pwd);
        when(userHandler.loadFromDB(any(), any(), any())).thenReturn(userFromDB);
        assertNotNull(loginService.validateUser(name, pwd));
    }

    @Test
    public void failedLogin(){
        when(userHandler.loadFromDB(any(), any(), any())).thenReturn(null);
        assertNull(loginService.validateUser("invalid", "invalid"));
    }

    @Test
    public void successLoginGetJson() throws JsonProcessingException {
        String name = "name";
        String pwd = "pwd";
        User userFromDB = new User(name, pwd);
        when(userHandler.loadFromDB(any(), any(), any())).thenReturn(userFromDB);

        String expectedResult = mapper.writeValueAsString(userFromDB);
        assertThat(loginService.getValidUser(name, pwd), is(equalTo(expectedResult)));
    }

    @Test
    public void failedLoginGetJson() {
        when(loginService.validateUser("invalid", "invalid")).thenReturn(null);
        assertNull(loginService.getValidUser("invalid", "invalid"));
    }
}