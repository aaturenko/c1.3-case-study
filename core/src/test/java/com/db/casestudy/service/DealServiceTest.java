package com.db.casestudy.service;


import com.db.casestudy.dto.TableSettingsDto;
import com.db.casestudy.handler.DealHandler;
import com.db.casestudy.model.Counterparty;
import com.db.casestudy.model.Deal;
import com.db.casestudy.model.Instrument;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


public class DealServiceTest {

    @InjectMocks
    DealService dealService;

    @Mock
    DealHandler dealHandler;

    ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = new ObjectMapper();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getDealPageJsonSuccess() throws JsonProcessingException, SQLException {
        Counterparty counterparty1 = new Counterparty(1, ", ", "", "");
        Instrument instrument1 = new Instrument(1, "");
        Deal deal1 = new Deal(1, "", counterparty1, instrument1, "", 1, 1);
        Deal deal2 = new Deal(2, "", counterparty1, instrument1, "", 1, 1);
        int page = 0;
        int size = 2;

        List<Deal> fromDBResult = Arrays.asList(deal1, deal2);
        String expectedResult = mapper.writeValueAsString(fromDBResult);

        TableSettingsDto tableSettings = new TableSettingsDto();
        when(dealHandler.loadFilteredDeals(any(), eq(tableSettings))).thenReturn(fromDBResult);
        assertThat(dealService.getDealPageJson(tableSettings), is(equalTo(expectedResult)));
    }

    @Test
    public void getDealPageJsonNull() {
        TableSettingsDto tableSettings = new TableSettingsDto();
        when(dealHandler.loadFilteredDeals(any(), eq(tableSettings))).thenReturn(null);
        assertNull(dealService.getDealPageJson(tableSettings));
    }

}
