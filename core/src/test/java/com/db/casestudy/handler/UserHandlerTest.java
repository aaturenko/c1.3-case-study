package com.db.casestudy.handler;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.model.User;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.*;

public class UserHandlerTest {

    UserHandler userHandler;

    Connection connection;

    @BeforeClass
    public static void initDB() throws SQLException, FileNotFoundException {
        try (Connection connection = DataSource.getInstance().getConnection()) {
            RunScript.execute(connection, new FileReader(new File("src/test/resources/db/db_grad.sql")));
        }
    }


    @Before
    public void setUp() throws Exception {
        userHandler = userHandler.getLoader();
        connection = DataSource.getInstance().getConnection();
    }

    @After
    public void tearDown() throws Exception {
        connection.close();
    }

    @Test
    public void loadFromDBTestSuccess() {
        String userid = "selvyn";
        String pwd = "gradprog2016";
        User user = userHandler.loadFromDB(connection, userid, pwd);
        assertEquals(userid, user.getUserID());
        assertEquals(pwd, user.getUserPwd());
    }

    @Test
    public void loadFromDBTestFailure() {
        String userid = "selvyn";
        String pwd = "";
        //the below should not fetch any users
        User user = userHandler.loadFromDB(connection, userid, pwd);
        assertThat(user, is(nullValue()));
    }


}