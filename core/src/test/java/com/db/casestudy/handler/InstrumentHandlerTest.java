package com.db.casestudy.handler;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.model.Instrument;
import org.h2.tools.RunScript;
import org.junit.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.*;

public class InstrumentHandlerTest {

    InstrumentHandler instrumentHandler;

    Connection connection;

    @BeforeClass
    public static void initDB() throws SQLException, FileNotFoundException {
        try (Connection connection = DataSource.getInstance().getConnection()) {
            RunScript.execute(connection, new FileReader(new File("src/test/resources/db/db_grad.sql")));
        }
    }

    @Before
    public void setUp() throws Exception {
        instrumentHandler = instrumentHandler.getLoader();
        connection = DataSource.getInstance().getConnection();
    }

    @After
    public void tearDown() throws Exception {
        connection.close();
    }

    @Test
    public void loadOneInstrumentTestValid() {
        int instrument_id = 1;
        String instrument_name = "Eclipse";
        Instrument instrument = instrumentHandler.loadFromDB(connection, instrument_id);
        assertEquals(instrument_id, instrument.getInstrumentID());
        assertEquals(instrument_name, instrument.getInstrumentName());
    }

    @Test
    public void loadOneInstrumentTestInvalid() {
        //use 0 for invalid instrument id
        Instrument instrument = instrumentHandler.loadFromDB(connection, 0);
        assertThat(instrument, is(nullValue()));
    }

    @Test
    public void loadRangeOfInstrumentsTestValid() {
        int instrument_id_low = 1;
        int instrument_id_high = 3;
        List<Instrument> instruments = instrumentHandler.loadFromDB(connection, instrument_id_low, instrument_id_high);
        assertThat(instruments, hasSize(3));
    }

    @Test
    public void loadRangeOfInstrumentsTestInvalid() {
        int instrument_id_low = 0;
        int instrument_id_high = 0;
        List<Instrument> instruments = instrumentHandler.loadFromDB(connection, instrument_id_low, instrument_id_high);
        assertThat(instruments, hasSize(0));
    }

    @Test
    public void loadAllInstrumentsTest() {
        List<Instrument> instruments = instrumentHandler.loadFromDB(connection);
        assertThat(instruments, hasSize(12));
    }
}