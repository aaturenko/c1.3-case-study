package com.db.casestudy.handler;


import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.dto.TableSettingsDto;
import com.db.casestudy.model.Deal;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DealHandlerTest {

    DealHandler dealHandler;

    Connection connection;

    @BeforeClass
    public static void initDB() throws SQLException, FileNotFoundException {
        try (Connection connection = DataSource.getInstance().getConnection()) {
            RunScript.execute(connection, new FileReader(new File("src/test/resources/db/db_grad.sql")));
        }
    }

    @Before
    public void setUp() throws Exception {
        dealHandler = DealHandler.getLoader();
        connection = DataSource.getInstance().getConnection();
    }

    @After
    public void tearDown() throws Exception {
        connection.close();
    }

    @Test
    public void loadPageableDealsFromDBTestFullPage() {
        int size = 10;
        List<Deal> deals = dealHandler.loadPageableDealsFromDB(connection, 0, size);
        assertThat(deals, hasSize(size));
    }

    @Test
    public void loadPageableDealsFromDBTestPartPage() {

        List<Deal> deals = dealHandler.loadPageableDealsFromDB(connection, 1, 1999);
        assertThat(deals, hasSize(1));
    }

    @Test
    public void loadFilteredDealsFromDBTestAllFields() {

        TableSettingsDto tableSettings = new TableSettingsDto(
                0,
                100,
                "Lina",
                "A",
                "Astronomica",
                "S",
                "deal_amount",
                "desc"
        );

        List<Deal> deals = dealHandler.loadFilteredDeals(connection, tableSettings);
        assertThat(deals, hasSize(14));
        assertThat(deals.get(0).getAmount(), greaterThan(deals.get(1).getAmount()));
        deals.forEach(deal -> assertEquals("Lina", deal.getCounterparty().getName()));
        deals.forEach(deal -> assertEquals("A", deal.getCounterparty().getStatus()));
        deals.forEach(deal -> assertEquals("Astronomica", deal.getInstrument().getInstrumentName()));
        deals.forEach(deal -> assertEquals("S", deal.getDealType()));
    }

    @Test
    public void loadFilteredDealsFromDBTestAsc() {

        TableSettingsDto tableSettings = new TableSettingsDto(
                0,
                100,
                null,
                "A",
                "Astronomica",
                "S",
                "deal_amount",
                "asc"
        );

        List<Deal> deals = dealHandler.loadFilteredDeals(connection, tableSettings);
        assertThat(deals, hasSize(95));
        assertThat(deals.get(0).getAmount(), lessThan(deals.get(1).getAmount()));
        assertTrue(deals.stream().anyMatch(deal -> "Lina".equals(deal.getCounterparty().getName())));
        deals.forEach(deal -> assertEquals("A", deal.getCounterparty().getStatus()));
        deals.forEach(deal -> assertEquals("Astronomica", deal.getInstrument().getInstrumentName()));
        deals.forEach(deal -> assertEquals("S", deal.getDealType()));
    }

    @Test
    public void loadFilteredDealsFromDBTestNullDesc() {

        TableSettingsDto tableSettings = new TableSettingsDto(
                0,
                100,
                null,
                null,
                null,
                null,
                "deal_amount",
                null
        );

        List<Deal> deals = dealHandler.loadFilteredDeals(connection, tableSettings);
        assertThat(deals, hasSize(100));
        assertThat(deals.get(0).getAmount(), greaterThan(deals.get(1).getAmount()));
    }

    @Test
    public void loadFilteredDealsFromDBTestDesc() {

        TableSettingsDto tableSettings = new TableSettingsDto(
                0,
                100,
                null,
                null,
                null,
                null,
                "deal_amount",
                "desc"
        );

        List<Deal> deals = dealHandler.loadFilteredDeals(connection, tableSettings);
        assertThat(deals, hasSize(100));
        assertThat(deals.get(0).getAmount(), greaterThan(deals.get(1).getAmount()));
    }

    @Test
    public void getTotalSizeTest() {
        int total = dealHandler.getTotalSize(connection);
        assertEquals(total, 2000);
    }

}
