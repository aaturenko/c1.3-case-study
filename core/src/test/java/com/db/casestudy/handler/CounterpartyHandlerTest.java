package com.db.casestudy.handler;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.dto.DealerNetTradesDto;
import com.db.casestudy.dto.DealerProfitLossDto;
import com.db.casestudy.dto.InstrumentAverageDto;
import com.db.casestudy.dto.LineChartDataDto;
import com.db.casestudy.model.Counterparty;
import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.*;

public class CounterpartyHandlerTest {

    CounterpartyHandler counterpartyHandler;

    Connection connection;

    @BeforeClass
    public static void initDB() throws SQLException, FileNotFoundException {
        try (Connection connection = DataSource.getInstance().getConnection()) {
            RunScript.execute(connection, new FileReader(new File("src/test/resources/db/db_grad.sql")));
        }
    }

    @Before
    public void setUp() throws Exception {
        counterpartyHandler = counterpartyHandler.getLoader();
        connection = DataSource.getInstance().getConnection();
    }

    @After
    public void tearDown() throws Exception {
        connection.close();
    }

    @Test
    public void findByNameTestValid() {
        int counterparty_id = 5;
        String counterparty_name = "Lina";
        String counterparty_status = "A";
        String counterparty_date = "2014-01-01 00:00:00";

        Counterparty counterparty = counterpartyHandler.findByName(connection, counterparty_name);
        assertEquals(counterparty_id, counterparty.getId());
        assertEquals(counterparty_name, counterparty.getName());
        assertEquals(counterparty_status, counterparty.getStatus());
        assertEquals(counterparty_date, counterparty.getDateRegistered());

    }

    @Test
    public void findByNameTestInvalid(){
        String counterparty_name = "";
        Counterparty counterparty = counterpartyHandler.findByName(connection, counterparty_name);
        assertThat(counterparty, is(nullValue()));
    }

    @Test
    public void findByIdTestValid() {
        int counterparty_id = 5;
        String counterparty_name = "Lina";
        String counterparty_status = "A";
        String counterparty_date = "2014-01-01 00:00:00";

        Counterparty counterparty = counterpartyHandler.findById(connection, counterparty_id);
        assertEquals(counterparty_id, counterparty.getId());
        assertEquals(counterparty_name, counterparty.getName());
        assertEquals(counterparty_status, counterparty.getStatus());
        assertEquals(counterparty_date, counterparty.getDateRegistered());
    }

    @Test
    public void findByIdTestInvalid() {
        int counterparty_id = 0;
        Counterparty counterparty = counterpartyHandler.findById(connection, counterparty_id);
        assertThat(counterparty, is(nullValue()));
    }

    @Test
    public void getLineChartDataTestValid() {
        int counterparty_id = 1;
        List<LineChartDataDto> data = counterpartyHandler.getLineChartData(connection, counterparty_id);
        assertThat(data, hasSize(387));
    }

    @Test
    public void getLineChartDataTestInvalid() {
        int counterparty_id = 0;
        List<LineChartDataDto> data = counterpartyHandler.getLineChartData(connection, counterparty_id);
        assertThat(data, hasSize(0));
    }

    @Test
    public void findDealerNetTradeTest() {
        List<DealerNetTradesDto> netTrades = counterpartyHandler.findDealerNetTrade(connection);
        assertThat(netTrades, hasSize(5));
    }

    @Test
    public void findDealerRealizedProfitLossTest() {
        ArrayList<DealerProfitLossDto> profitLoss = counterpartyHandler.findDealerRealisedProfitLoss(connection);
        assertThat(profitLoss, hasSize(12));
    }

    @Test
    public void findInstrumentAverageTest() {
        ArrayList<InstrumentAverageDto> avg = counterpartyHandler.findInstrumentAverage(connection);
        assertThat(avg, hasSize(12));
    }


}