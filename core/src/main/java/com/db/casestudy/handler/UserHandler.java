/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.casestudy.handler;

import com.db.casestudy.dto.LineChartDataDto;
import com.db.casestudy.model.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Selvyn
 */
public class UserHandler
{
    static  private UserHandler itsSelf = null;
    private static Logger log = Logger.getLogger(UserHandler.class);
    
    private UserHandler(){}
    
    static  public  UserHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new UserHandler();
        return itsSelf;
    }

    private User buildUser(ResultSet rs) throws SQLException
    {
        return new User(
                rs.getString("user_id"),
                rs.getString("user_pwd"));
    }

    public  User  loadFromDB( Connection theConnection, String userid, String pwd )
    {
        User result = null;
        try
        {
            String sbQuery = "select * from users where user_id=? and user_pwd=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setString(1, userid);
            stmt.setString(2, pwd);
            ResultSet rs = stmt.executeQuery();
            
            if( rs.next() )
            {
                result = buildUser(rs);
                log.info(result.getUserID() + "//" + result.getUserPwd() );
            }
        } 
        catch (SQLException ex)
        {
            log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
        
        return result;
    }


}
