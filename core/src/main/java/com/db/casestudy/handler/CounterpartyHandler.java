
package com.db.casestudy.handler;

import com.db.casestudy.dto.*;
import com.db.casestudy.model.Counterparty;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CounterpartyHandler
{
    private static Logger log = Logger.getLogger(CounterpartyHandler.class);
    static  private CounterpartyHandler itsSelf = null;

    private CounterpartyHandler(){}

    static  public CounterpartyHandler getLoader()
    {
        if( itsSelf == null )
            itsSelf = new CounterpartyHandler();
        return itsSelf;
    }

    private String getCounterpartyByIdQuery(int key) {
        return "select * from counterparty where counterparty_id=" + String.valueOf(key);
    }

    private String getCounterpartyByNameQuery(String key) {
        return "select * from counterparty where counterparty_name='" + String.valueOf(key) + "'";
    }

    public Counterparty findByName(Connection theConnection, String name) {
        return loadFromDB(
                theConnection,
                getCounterpartyByNameQuery(name)
        );
    }

    public Counterparty findById(Connection theConnection, int key ) {
        return loadFromDB(
                theConnection,
                getCounterpartyByIdQuery(key)
        );
    }

    public List<LineChartDataDto> getLineChartData(Connection theConnection, int counterpartyId) {

        List<LineChartDataDto> result = null;
        try
        {
            String sbQuery = "SELECT deal_time, deal_amount FROM deal\n" +
                    "join counterparty c on deal_counterparty_id=c.counterparty_id\n" +
                    "where deal_counterparty_id=?\n"+
                    "order by deal_time asc";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            stmt.setInt(1, counterpartyId);
            ResultSet rs = stmt.executeQuery();

            result = new ArrayList<>();
            while( rs.next() )
            {
                LineChartDataDto dataDto = new LineChartDataDto(
                        rs.getString("deal_time"),
                        rs.getString("deal_amount"));
                result.add(dataDto);
            }
        }
        catch (SQLException ex)
        {
            log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
        return result;
    }

    private Counterparty loadFromDB(Connection theConnection, String sbQuery)
    {
        Counterparty result = null;
        try {
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();

            while( rs.next() )
            {
                result = buildCounterparty(rs);
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }

        return result;
    }

    private Counterparty buildCounterparty(ResultSet rs) throws SQLException {
        return new Counterparty(
                rs.getInt("counterparty_id"),
                rs.getString("counterparty_name"),
                rs.getString("counterparty_status"),
                rs.getString("counterparty_date_registered")
        );
    }

    public List<DealerNetTradesDto> findDealerNetTrade(Connection theConnection) {

        List<DealerNetTradesDto> dealerNetTradesDtos = null;

        try
        {
            String sbQuery = "SELECT counterparty_name, instrument_name, \n" +
                    "\t(SUM(case when deal_type = \'B\' then deal_quantity else 0 end) - SUM(case when deal_type = \'S\' then deal_quantity else 0 end) ) AS NETTRADE\n" +
                    "    FROM instrument i INNER JOIN deal d\n" +
                    "\t\tON i.instrument_id = d.deal_instrument_id\n" +
                    "\tINNER JOIN counterparty c\n" +
                    "\t\tON d.deal_counterparty_id = c.counterparty_id\n" +
                    "\tGROUP BY counterparty_name, instrument_name";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();

            Map<String, List<NetTradeDto>> result = new HashMap<>();

            while( rs.next() )
            {
                String counterPartyName = rs.getString("counterparty_name");
                String instrumentName = rs.getString("instrument_name");
                int netTrade = rs.getInt("NETTRADE");

                ArrayList<NetTradeDto> list = new ArrayList<>();
                list.add(new NetTradeDto(instrumentName, netTrade));
                result.merge(counterPartyName, list,
                        (v1, v2) -> {
                            v1.addAll(v2);
                            return v1;
                        }
                );
            }

            dealerNetTradesDtos = result.entrySet().stream()
                    .map(k -> new DealerNetTradesDto(k.getKey(), k.getValue()))
                    .collect(Collectors.toList());
        }
        catch (SQLException ex) {
            log.error(ex.getMessage());
        }


        return dealerNetTradesDtos;
    }

    public ArrayList<DealerProfitLossDto> findDealerRealisedProfitLoss(Connection theConnection) {

        ArrayList<DealerProfitLossDto> result = new ArrayList<>();

        try
        {
            String sbQuery = "SELECT i.instrument_name, \n" +
                    "(SUM(case when deal_type = \'S\' then deal_quantity*deal_amount else 0 end) - SUM(case when deal_type = \'B\' then deal_quantity*deal_amount else 0 end) ) AS realised_profit_loss\n" +
                    "FROM counterparty c JOIN deal d\n" +
                    "ON c.counterparty_id = d.deal_counterparty_id\n" +
                    "JOIN instrument i ON d.deal_instrument_id=i.instrument_id\n" +
                    "GROUP BY i.instrument_name";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();

            while( rs.next() )
            {
                String counterPartyName = rs.getString("instrument_name");
                int profitLoss = rs.getInt("realised_profit_loss");
                result.add(new DealerProfitLossDto(counterPartyName, profitLoss));

            }
        }
        catch (SQLException ex) {
            log.error(ex.getMessage());
        }

        return result;
    }

    public ArrayList<InstrumentAverageDto> findInstrumentAverage(Connection theConnection) {

        ArrayList<InstrumentAverageDto> result = new ArrayList<>();

        try
        {
            String sbQuery = "select X.instrument_name as instrument, average_buy, average_sell from \n" +
                    "(SELECT instrument_name, " +
                        "SUM(deal_amount*deal_quantity)/SUM(deal_quantity) as average_buy\n" +
                    " FROM instrument i INNER JOIN deal d\n" +
                    " WHERE i.instrument_id = d.deal_instrument_id AND d.deal_type = \'B\'\n" +
                    "GROUP BY instrument_name) X\n" +
                    "join\n" +
                    "(SELECT instrument_name, " +
                        "SUM(deal_amount*deal_quantity)/SUM(deal_quantity) as average_sell\n" +
                    "FROM instrument i INNER JOIN deal d\n" +
                    "WHERE i.instrument_id = d.deal_instrument_id AND d.deal_type = \'S\'\n" +
                    "GROUP BY instrument_name) Y\n" +
                    "ON X.instrument_name = Y.instrument_name";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();

            while( rs.next() )
            {
                String instrumentName = rs.getString("instrument");
                int averageBuy = rs.getInt("average_buy");
                int averageSell = rs.getInt("average_sell");

                result.add(new InstrumentAverageDto(
                        instrumentName,
                        averageBuy,
                        averageSell
                ));
            }
        }
        catch (SQLException ex) {
            log.error(ex.getMessage());
        }

        return result;
    }

    public ArrayList<DealerEffectiveDto> findDealerEffectiveProfit(Connection theConnection) {

        ArrayList<DealerEffectiveDto> result = new ArrayList<>();


        try
        {
            String sbQuery =
                    "select \n" +
                    "* from\n" +
                    "(SELECT i.instrument_name, i.instrument_id as instrument_id, SUM(deal_quantity),\n" +
                    "(SUM(case when deal_type = \"S\" then deal_quantity*deal_amount else 0 end) - SUM(case when deal_type = \"B\" then deal_quantity*deal_amount else 0 end) ) AS realised_profit_loss\n" +
                    "FROM db_grad_cs_1917.counterparty c INNER JOIN db_grad_cs_1917.deal d\n" +
                    "ON c.counterparty_id = d.deal_counterparty_id\n" +
                    "JOIN instrument i ON d.deal_instrument_id=i.instrument_id\n" +
                    "GROUP BY i.instrument_id) t1\n" +
                    "join\n" +
                    "(SELECT deal_instrument_id as instrument_id, MAX(deal_time) as deal_time, deal_amount, deal_type\n" +
                    "FROM deal s1\n" +
                    "WHERE deal_time = (SELECT MAX(s2.deal_time) FROM deal s2 \n" +
                    "WHERE s1.deal_instrument_id = s2.deal_instrument_id AND s1.deal_type = s2.deal_type)\n" +
                    "GROUP BY deal_instrument_id, deal_amount, deal_type) t2\n" +
                    "on t1.instrument_id = t2.instrument_id\n" +
                    "join\n" +
                    "(SELECT instrument_id, \n" +
                    "\t(SUM(case when deal_type = \"B\" then deal_quantity else 0 end) - SUM(case when deal_type = \"S\" then deal_quantity else 0 end) ) AS NETTRADE\n" +
                    "    FROM db_grad_cs_1917.instrument i INNER JOIN db_grad_cs_1917.deal d\n" +
                    "\t\tON i.instrument_id = d.deal_instrument_id\n" +
                    "\tINNER JOIN counterparty c\n" +
                    "\t\tON d.deal_counterparty_id = c.counterparty_id\n" +
                    "\tGROUP BY instrument_id) t3\n" +
                    "on t2.instrument_id = t3.instrument_id;";


            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();


            while( rs.next() )
            {

                int effectiveProfit = 0;

                String instrumentName = rs.getString("instrument_name");
                int realisedProfit = rs.getInt("realised_profit_loss");
                int netTrade = rs.getInt("NETTRADE");
                int dealAmount = rs.getInt("deal_amount");
                String dealType = rs.getString("deal_type");


                if(netTrade < 0 & dealType.equals("B")) {
                    effectiveProfit = realisedProfit - netTrade * dealAmount;
                } else if(netTrade > 0 & dealType.equals("S")) {
                    effectiveProfit = realisedProfit + netTrade * dealAmount;
                } else {
                    continue;
                }




                result.add(new DealerEffectiveDto(
                        instrumentName,
                        effectiveProfit
                ));


            }


        }
        catch (SQLException ex) {
            log.error(ex.getMessage());
        }

        return result;

    }

}
