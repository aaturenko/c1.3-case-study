/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.casestudy.handler;

import com.db.casestudy.dto.TableSettingsDto;
import com.db.casestudy.model.Counterparty;
import com.db.casestudy.model.Deal;
import com.db.casestudy.model.Instrument;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DealHandler
{
    private static Logger log = Logger.getLogger(DealHandler.class);
    static  private DealHandler itsSelf = null;

    private DealHandler(){}

    static  public DealHandler getLoader()
    {
        if( itsSelf == null )
            itsSelf = new DealHandler();
        return itsSelf;
    }

    private Deal buildDeal(ResultSet rs, Connection theConnection) throws SQLException
    {
        Instrument instrument = InstrumentHandler.getLoader().loadFromDB(theConnection, rs.getInt("deal_instrument_id"));
        Counterparty counterparty = CounterpartyHandler.getLoader().findById(theConnection, rs.getInt("deal_counterparty_id"));
        return new Deal(
                rs.getInt("deal_id"),
                rs.getString("deal_time"),
                counterparty,
                instrument,
                rs.getString("deal_type"),
                rs.getDouble("deal_amount"),
                rs.getInt("deal_quantity")
        );
    }

    public List<Deal> loadPageableDealsFromDB(Connection theConnection, int page, int size)  {
        List<Deal> result = null;
        try {
            PreparedStatement preparedStatement = prepareStatementForPageableDeals(theConnection, page, size);
            result = loadListFromDB(theConnection, preparedStatement);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    public List<Deal> loadFilteredDeals(Connection theConnection, TableSettingsDto tableSettings)  {
        List<Deal> result = null;
        try {
            PreparedStatement preparedStatement = prepareStatementForFilteredDeals(theConnection, tableSettings);
            result = loadListFromDB(theConnection, preparedStatement);
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
        return result;
    }

    private PreparedStatement prepareStatementForFilteredDeals(Connection theConnection, TableSettingsDto tableSettings) throws SQLException {
        String instrumentName = tableSettings.getInstrumentName();
        String counterpartyName = tableSettings.getCounterpartyName();
        String dealType = tableSettings.getDealType();
        String counterpartyStatus = tableSettings.getCounterpartyStatus();
        String sortField = tableSettings.getSortField();
        int page = tableSettings.getPage();
        int size = tableSettings.getSize();

        StringBuilder sbQuery = new StringBuilder()
                .append("select deal_id, deal_time, deal_counterparty_id, deal_instrument_id, deal_type, deal_amount, deal_quantity\n")
                .append("from deal\n")
                .append("join instrument i on deal_instrument_id=i.instrument_id\n")
                .append("join counterparty c on deal_counterparty_id=c.counterparty_id\n");

        if (instrumentName != null && !instrumentName.isEmpty()) sbQuery.append("and i.instrument_name = '").append(instrumentName).append("'");
        if (counterpartyName != null && !counterpartyName.isEmpty()) sbQuery.append("\nand c.counterparty_name = '").append(counterpartyName).append("'");
        if (dealType != null && !dealType.isEmpty()) sbQuery.append("\nand deal_type = '").append(dealType).append("'");
        if (counterpartyStatus != null && !counterpartyStatus.isEmpty()) sbQuery.append("\nand c.counterparty_status = '").append(counterpartyStatus).append("'");
        if (sortField != null&& !sortField.isEmpty()) {
            String order = tableSettings.getOrder() != null && "asc".equals(tableSettings.getOrder()) ? "asc" : "desc";
            sbQuery.append("\norder by ").append(sortField).append(" ").append(order);
        }
        sbQuery.append("\nlimit ").append(size).append(" offset ").append(page*size);

        return theConnection.prepareStatement(sbQuery.toString());
    }

    public Integer getTotalSize(Connection theConnection)  {
        int result = 0;
        try {
            PreparedStatement preparedStatement = theConnection.prepareStatement("SELECT count(*) as total FROM deal");
            ResultSet rs = preparedStatement.executeQuery();
            if( rs.next() ) {
                result = rs.getInt("total");
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
        return result;
    }

    private List<Deal> loadListFromDB(Connection theConnection, PreparedStatement stmt) throws SQLException {
        List<Deal> result;

        ResultSet rs = stmt.executeQuery();
        result = new ArrayList<>();
        while( rs.next() ) {
            result.add(buildDeal(rs, theConnection));
        }

        return result;
    }

    private PreparedStatement prepareStatementForPageableDeals(Connection theConnection, int page, int size) throws SQLException {
        String sbQuery = "select * from deal d limit ? offset ?";
        PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
        stmt.setInt(1, size);
        stmt.setInt(2, page*size);

        return stmt;
    }



}
