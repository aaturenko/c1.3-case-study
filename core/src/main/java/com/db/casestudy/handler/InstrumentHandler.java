/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.casestudy.handler;

import com.db.casestudy.model.Instrument;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class InstrumentHandler
{
    private static Logger log = Logger.getLogger(InstrumentHandler.class);
    static  private InstrumentHandler itsSelf = null;
    
    private InstrumentHandler(){}
    
    static  public  InstrumentHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new InstrumentHandler();
        return itsSelf;
    }

    private Instrument buildInstrument(ResultSet rs) throws SQLException
    {
        Instrument result = new Instrument(
                rs.getInt("instrument_id"),
                rs.getString("instrument_name") );

        return result;
    }

    public  Instrument  loadFromDB(Connection theConnection, int key )
    {
        Instrument result = null;
        try
        {
            String sbQuery = "select * from instrument where instrument_id=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, key);
            ResultSet rs = stmt.executeQuery();
            
            while( rs.next() )
            {
                result = buildInstrument(rs);
                log.debug( result.getInstrumentID() + "//" + result.getInstrumentName() );
            }
        } 
        catch (SQLException ex)
        {
            log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
        
        return result;
    }

    public  ArrayList<Instrument>  loadFromDB(Connection theConnection, int lowerKey, int upperKey )
    {
        ArrayList<Instrument> result = new ArrayList<Instrument>();
        Instrument theInstrument = null;
        try
        {
            String sbQuery = "select * from instrument where instrument_id>=? and instrument_id<=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, lowerKey);
            stmt.setInt(2, upperKey);
            ResultSet rs = stmt.executeQuery();
            
            while( rs.next() )
            {
                theInstrument = buildInstrument(rs);
                log.debug( theInstrument.getInstrumentID() + "//" + theInstrument.getInstrumentName() );
                result.add(theInstrument);
            }
        } 
        catch (SQLException ex)
        {
            log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
        
        return result;
    }
    
    public  ArrayList<Instrument>  loadFromDB(Connection theConnection )
    {
        ArrayList<Instrument> result = new ArrayList<Instrument>();
        Instrument theInstrument = null;
        try
        {
            String sbQuery = "select * from instrument";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            while( rs.next() )
            {
                theInstrument = buildInstrument(rs);
                log.debug( theInstrument.getInstrumentID() + "//" + theInstrument.getInstrumentName() );
                result.add(theInstrument);
            }
        } 
        catch (SQLException ex)
        {
            log.error(ex.getMessage());
            throw new RuntimeException(ex);
        }
        
        return result;
    }

}
