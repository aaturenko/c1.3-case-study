package com.db.casestudy.dto;

import java.util.List;

public class DealerNetTradesDto {

    private String counterpartyName;
    private List<NetTradeDto> netTrades;

    public DealerNetTradesDto(String counterpartyName, List<NetTradeDto> netTrades) {
        this.counterpartyName = counterpartyName;
        this.netTrades = netTrades;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public List<NetTradeDto> getNetTrades() {
        return netTrades;
    }

    public void setNetTrades(List<NetTradeDto> netTrades) {
        this.netTrades = netTrades;
    }
}
