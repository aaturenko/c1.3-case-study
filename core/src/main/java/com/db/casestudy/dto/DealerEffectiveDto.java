package com.db.casestudy.dto;

import java.util.List;

public class DealerEffectiveDto {

    private String instrumentName;
    private int effectiveProfit;

    public DealerEffectiveDto(String instrumentName, int effectiveProfit) {
        this.instrumentName = instrumentName;
        this.effectiveProfit = effectiveProfit;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public int getEffectiveProfit() {
        return effectiveProfit;
    }

    public void setEffectiveProfit(int effectiveProfit) {
        this.effectiveProfit = effectiveProfit;
    }
}
