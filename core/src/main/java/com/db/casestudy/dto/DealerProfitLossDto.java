package com.db.casestudy.dto;

public class DealerProfitLossDto {

    private String counterpartyName;
    private int realisedProfit;

    public DealerProfitLossDto(String counterpartyName, int realisedProfit) {
        this.counterpartyName = counterpartyName;
        this.realisedProfit = realisedProfit;
    }

    public int getRealisedProfit() {
        return realisedProfit;
    }

    public void setRealisedProfit(int realisedProfit) {
        this.realisedProfit = realisedProfit;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }
}
