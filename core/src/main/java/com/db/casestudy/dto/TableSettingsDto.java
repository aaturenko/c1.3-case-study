package com.db.casestudy.dto;

public class TableSettingsDto {
    private int page;
    private int size;
    private String counterpartyName;
    private String counterpartyStatus;
    private String instrumentName;
    private String dealType;
    private String sortField;
    private String order;

    public TableSettingsDto() {
    }

    public TableSettingsDto(int page, int size, String counterpartyName, String counterpartyStatus, String instrumentName, String dealType, String sortField, String order) {
        this.page = page;
        this.size = size;
        this.counterpartyName = counterpartyName;
        this.counterpartyStatus = counterpartyStatus;
        this.instrumentName = instrumentName;
        this.dealType = dealType;
        this.sortField = sortField;
        this.order = order;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getCounterpartyStatus() {
        return counterpartyStatus;
    }

    public void setCounterpartyStatus(String counterpartyStatus) {
        this.counterpartyStatus = counterpartyStatus;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
