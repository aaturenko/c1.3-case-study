package com.db.casestudy.dto;

public class NetTradeDto {

    private String instrumentName;
    private int netTrade;

    public NetTradeDto(String instrumentName, int netTrade) {
        this.instrumentName = instrumentName;
        this.netTrade = netTrade;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public int getNetTrade() {
        return netTrade;
    }

    public void setNetTrade(int netTrade) {
        this.netTrade = netTrade;
    }
}
