package com.db.casestudy.dto;

public class InstrumentAverageDto {

    private String instrument_name;
    private int averageBuy;
    private int averageSell;

    public InstrumentAverageDto(String instrument_name, int averageBuy, int averageSell) {
        this.instrument_name = instrument_name;
        this.averageBuy = averageBuy;
        this.averageSell = averageSell;
    }

    public String getInstrument_name() {
        return instrument_name;
    }

    public void setInstrument_name(String instrument_name) {
        this.instrument_name = instrument_name;
    }

    public int getAverageBuy() {
        return averageBuy;
    }

    public void setAverageBuy(int averageBuy) {
        this.averageBuy = averageBuy;
    }

    public int getAverageSell() {
        return averageSell;
    }

    public void setAverageSell(int averageSell) {
        this.averageSell = averageSell;
    }
}
