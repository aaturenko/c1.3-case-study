package com.db.casestudy.dto;

public class LineChartDataDto {

    private String amount;

    private String dealTime;

    public LineChartDataDto(String dealTime, String amount) {
        this.amount = amount;
        this.dealTime = dealTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDealTime() {
        return dealTime;
    }

    public void setDealTime(String dealTime) {
        this.dealTime = dealTime;
    }
}
