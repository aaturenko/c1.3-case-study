package com.db.casestudy.service;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.handler.UserHandler;
import com.db.casestudy.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class LoginService {

    private static  final Logger log = Logger.getLogger(LoginService.class);
    private UserHandler theUserHandler = UserHandler.getLoader();
    private ObjectMapper mapper = new ObjectMapper();

    public User validateUser(String name, String password) {
        try (Connection connection = DataSource.getInstance().getConnection()){
            return theUserHandler.loadFromDB(connection, name, password);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

	public String getValidUser(String name, String password) {

         String result = null;

         User theUser = validateUser(name, password);
         if( theUser != null) {
            try {
                result = mapper.writeValueAsString(theUser);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
                throw new RuntimeException(e);
            }
        }

        return result;
    }

}
