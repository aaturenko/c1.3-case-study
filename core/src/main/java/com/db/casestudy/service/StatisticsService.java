package com.db.casestudy.service;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.dto.LineChartDataDto;
import com.db.casestudy.handler.CounterpartyHandler;
import com.db.casestudy.model.Counterparty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class StatisticsService {

    private static  final Logger log = Logger.getLogger(RequirementService.class);

    private DataSource dataSource;

    private CounterpartyHandler counterpartyHandler;

    public StatisticsService() {
        dataSource = DataSource.getInstance();
        counterpartyHandler = CounterpartyHandler.getLoader();
    }

    public String getLineChartJson(String counterpartyName) throws JsonProcessingException {
        String result;

        Counterparty dealer = null;
        try (Connection connection = dataSource.getConnection()){
            dealer = counterpartyHandler.findByName(connection, counterpartyName);
            if (dealer != null) {
                List<LineChartDataDto> lineChartData = counterpartyHandler.getLineChartData(connection, dealer.getId());
                result = new ObjectMapper().writeValueAsString(lineChartData);
            } else {
                throw new RuntimeException(String.format("Dealer with name %s does not exists", counterpartyName));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return result;
    }

    public String getPieChartJson() {
        return null;
    }
}
