package com.db.casestudy.service;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.dto.*;
import com.db.casestudy.handler.CounterpartyHandler;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RequirementService {

    private static  final Logger log = Logger.getLogger(RequirementService.class);

    private DataSource dataSource;

    private CounterpartyHandler counterpartyHandler;

    public RequirementService() {
        dataSource = DataSource.getInstance();
        counterpartyHandler = CounterpartyHandler.getLoader();
    }

    public List<DealerNetTradesDto> getDealerNetTrade() {
        try (Connection connection = dataSource.getConnection()){
            return counterpartyHandler.findDealerNetTrade(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public ArrayList<DealerProfitLossDto> getDealerProfitLoss() {
        try (Connection connection = dataSource.getConnection()){
            return counterpartyHandler.findDealerRealisedProfitLoss(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public List<InstrumentAverageDto> getInstrumentAverage() {
        try (Connection connection = dataSource.getConnection()){
            return counterpartyHandler.findInstrumentAverage(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public List<DealerEffectiveDto> getEffectiveProfit() {
        try (Connection connection = dataSource.getConnection()){
            return counterpartyHandler.findDealerEffectiveProfit(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
