package com.db.casestudy.service;

import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.dto.TableSettingsDto;
import com.db.casestudy.handler.DealHandler;
import com.db.casestudy.model.Deal;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.List;

public class DealService {
    private static  final Logger log = Logger.getLogger(DealService.class);
    private DealHandler dealHandler;
    private ObjectMapper mapper;
    private DataSource dataSource;

    public DealService() {
        dealHandler = DealHandler.getLoader();
        mapper = new ObjectMapper();
        dataSource = DataSource.getInstance();
    }

    public String getDealPageJson(TableSettingsDto tableSettings) {

        String result = null;

        List<Deal> dealPage = findDealPage(tableSettings);
        if( dealPage != null) {
            try {
                result = mapper.writeValueAsString(dealPage);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
        }

        return result;
    }

    public List<Deal> findDealPage(TableSettingsDto tableSettings){

        List<Deal> result = null;

        try (Connection connection = dataSource.getConnection()){
            result = dealHandler.loadFilteredDeals(connection, tableSettings);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return result;
    }

    public Integer getTotalSize() {
        int result = 0;
        try (Connection connection = dataSource.getConnection()){
            result = dealHandler.getTotalSize(connection);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return result;
    }
}
