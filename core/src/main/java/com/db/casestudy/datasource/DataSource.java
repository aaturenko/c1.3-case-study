package com.db.casestudy.datasource;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {

    private static  final Logger log = Logger.getLogger(DataSource.class);
    private static DataSource datasource;
    private javax.sql.DataSource cpds;
    private DataSourceManager dsManager;

    private DataSource() {
        dsManager = new DataSourceManager();
        cpds = dsManager.getDataSource();
        log.info("Database connection initialized for Application.");
    }

    public static DataSource getInstance() {
        if (datasource == null) {
            datasource = new DataSource();
            return datasource;
        } else {
            return datasource;
        }
    }

    public boolean checkDBConnection() {
        boolean result = false;
        try (Connection connection = getConnection()) {
            result = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Connection getConnection() throws SQLException {
            return cpds.getConnection();
    }

}