package com.db.casestudy.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DataSourceManager {

    private static org.apache.log4j.Logger log = Logger.getLogger(DataSourceManager.class);
    private ComboPooledDataSource dataSource;

    public DataSourceManager() {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("db.properties")){
            Properties properties = new Properties();
            properties.load(inputStream);

            String dbDriver = properties.getProperty("dbDriver");
            String dbPath = properties.getProperty("dbPath");
            String dbUser = properties.getProperty("dbUser");
            String dbPwd = properties.getProperty("dbPwd");

            Class.forName( dbDriver );

            dataSource = new ComboPooledDataSource();
            dataSource.setDriverClass("com.mysql.jdbc.Driver");
            dataSource.setJdbcUrl(dbPath);
            dataSource.setUser(dbUser);
            dataSource.setPassword(dbPwd);
            dataSource.setMinPoolSize(1);
            dataSource.setMaxPoolSize(20);
            dataSource.setAcquireIncrement(1);
            dataSource.setAutoCommitOnClose(true);
        } catch (PropertyVetoException | IOException | ClassNotFoundException e) {
            log.error(e.getMessage());
        }
    }

    public DataSource getDataSource(){
        return dataSource;
    }
}
