package com.db.casestudy.model;

public class Counterparty {
    int id;
    String name;
    String status;
    String dateRegistered;

    public Counterparty(int id, String name, String status, String dateRegistered) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.dateRegistered = dateRegistered;
    }

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }


    public String getDateRegistered() {
        return dateRegistered;
    }

}
