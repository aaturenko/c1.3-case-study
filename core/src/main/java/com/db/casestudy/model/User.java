/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.casestudy.model;

/**
 *
 * @author Selvyn
 */
public class User
{
    private String  itsUserID;
    private String  itsUserPwd;
    
    public  User( String userid, String pwd )
    {
        itsUserID = userid;
        itsUserPwd = pwd;
    }

    public String getUserID()
    {
        return itsUserID;
    }
    public String getUserPwd()
    {
        return itsUserPwd;
    }

}
