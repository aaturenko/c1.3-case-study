package com.db.casestudy.model;

public class Deal  {
    int id;
    String dealTime;
    Counterparty counterparty;
    Instrument instrument;
    String dealType;
    double amount;
    int quantity;

    public Deal(int id, String dealTime, Counterparty counterparty, Instrument instrument, String dealType, double amount, int quantity) {
        this.id = id;
        this.dealTime = dealTime;
        this.counterparty = counterparty;
        this.instrument = instrument;
        this.dealType = dealType;
        this.amount = amount;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public String getDealTime() {
        return dealTime;
    }

    public Counterparty getCounterparty() {
        return counterparty;
    }


    public Instrument getInstrument() {
        return instrument;
    }

    public String getDealType() {
        return dealType;
    }

    public double getAmount() {
        return amount;
    }


    public int getQuantity() {
        return quantity;
    }

}
