package com.db.casestudy;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ResponseUtil {

    private static Logger log = Logger.getLogger(ResponseUtil.class);
    public static final String CONTEXT_PATH = "";
    public static final String LOGIN_END_POINT = CONTEXT_PATH + "/login";
    public static final String WELCOME_END_POINT = CONTEXT_PATH + "/welcome";

    public static void addJsonToResponse(HttpServletResponse response, String json) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        out.print(json);
        out.flush();
    }

    public static void setResponse(HttpServletResponse response, String result) throws IOException {

        if (result != null) {
            ResponseUtil.addJsonToResponse(
                    response,
                    result
            );
        } else {
            response.setStatus(500);
            String msg = "Error request";
            response.setHeader("message", msg);
            ResponseUtil.addJsonToResponse(response, msg);
        }
    }

}
