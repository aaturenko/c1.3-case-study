package com.db.casestudy.filter;

import com.db.casestudy.ResponseUtil;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.function.Predicate;
import java.util.stream.Stream;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {

	private static final Logger log = Logger.getLogger(Filter.class);

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest servletRequest,
						 ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String uri = request.getRequestURI();
		log.debug("Requested Resource::"+uri);

		HttpSession session = request.getSession(false);
		Cookie[] cookies = request.getCookies();

		if (checkAuth.test(session)|| checkCookie.test(cookies)) {
			if (uri.equals(ResponseUtil.LOGIN_END_POINT) ){
				response.sendRedirect(ResponseUtil.WELCOME_END_POINT);
			} else {
				chain.doFilter(request, servletResponse);
			}
		} else {
			if (uri.equals(ResponseUtil.LOGIN_END_POINT) || uri.endsWith("css") || uri.startsWith(ResponseUtil.CONTEXT_PATH + "/logo") || uri.equals(ResponseUtil.CONTEXT_PATH + "/checkDB")) {
				chain.doFilter(request, servletResponse);
			} else {
				response.sendRedirect(ResponseUtil.LOGIN_END_POINT);
			}
		}

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	private Predicate<HttpSession> checkAuth =
			session -> session != null && !session.isNew() && session.getAttribute("user") != null;

	private Predicate<Cookie[]> checkCookie =
			cookies -> cookies != null && Stream.of(cookies).anyMatch(c -> c.getName().equals("user"));


}
