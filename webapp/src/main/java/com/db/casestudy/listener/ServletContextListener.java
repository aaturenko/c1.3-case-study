package com.db.casestudy.listener;

import com.db.casestudy.datasource.DataSourceManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

public class ServletContextListener implements javax.servlet.ServletContextListener {

    private static  final Logger log = Logger.getLogger(ServletContextListener.class);

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        ctx.setAttribute("dataSource", new DataSourceManager().getDataSource());
        log.info("Database connection initialized for Application.");
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.info("Database connection closed for Application.");
    }
}
