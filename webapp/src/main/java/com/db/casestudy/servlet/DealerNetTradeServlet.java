package com.db.casestudy.servlet;

import com.db.casestudy.ResponseUtil;
import com.db.casestudy.dto.DealerNetTradesDto;
import com.db.casestudy.dto.NetTradeDto;
import com.db.casestudy.service.RequirementService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/dealer/nettrade")
public class DealerNetTradeServlet extends HttpServlet {

    private RequirementService requirementService = new RequirementService();

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        List<DealerNetTradesDto> dealerNetTrade = requirementService.getDealerNetTrade();
        String netTradeJson = new ObjectMapper().writeValueAsString(dealerNetTrade);
        ResponseUtil.setResponse(response, netTradeJson);

    }



}