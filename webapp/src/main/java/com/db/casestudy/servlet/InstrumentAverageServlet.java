package com.db.casestudy.servlet;

import com.db.casestudy.ResponseUtil;
import com.db.casestudy.dto.InstrumentAverageDto;
import com.db.casestudy.service.RequirementService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/instrument/average")
public class InstrumentAverageServlet extends HttpServlet {

    private RequirementService requirementService = new RequirementService();

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        List<InstrumentAverageDto> dealerAverageBuy = requirementService.getInstrumentAverage();
        String averageBuyJson = new ObjectMapper().writeValueAsString(dealerAverageBuy);
        ResponseUtil.setResponse(response, averageBuyJson);
    }

}