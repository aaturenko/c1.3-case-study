package com.db.casestudy.servlet;


import com.db.casestudy.ResponseUtil;
import com.db.casestudy.dto.TableSettingsDto;
import com.db.casestudy.service.DealService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/deal")
public class DealServlet extends HttpServlet {

	private DealService dealService = new DealService();

    protected void doGet(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        ResponseUtil.setResponse(response, dealService.getTotalSize().toString());

    }

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		TableSettingsDto tableSettings = getTableSettings(request);
		String dealPage = dealService.getDealPageJson(tableSettings);
		ResponseUtil.setResponse(response, dealPage);

	}

	private TableSettingsDto getTableSettings(HttpServletRequest request) {
		String page = request.getParameter("page");
		String size = request.getParameter("size");
		String counterpartyName = request.getParameter("counterparty_name");
		String counterpartyStatus = request.getParameter("counterparty_status");
		String instrumentName = request.getParameter("instrument_name");
		String dealType = request.getParameter("deal_type");
		String sortField = request.getParameter("sort_field");
		String order = request.getParameter("order");

		return new TableSettingsDto(
				page != null && !page.isEmpty()? Integer.valueOf(page) : 0,
				size != null && !size.isEmpty() ? Integer.valueOf(size) : 10,
				counterpartyName,
				counterpartyStatus,
				instrumentName,
				dealType,
				sortField,
				order
		);
	}


}