package com.db.casestudy.servlet;

import com.db.casestudy.ResponseUtil;
import com.db.casestudy.service.StatisticsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/dealer/lineChart")
public class StatisticsServlet extends HttpServlet {

    private StatisticsService statisticsService;

    public StatisticsServlet() {
        this.statisticsService = new StatisticsService();
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String counterpartyName = request.getParameter("counterparty_name");
        String lineChartData = statisticsService.getLineChartJson(counterpartyName);
        ResponseUtil.setResponse(response, lineChartData);


    }
}
