package com.db.casestudy.servlet;


import com.db.casestudy.ResponseUtil;
import com.db.casestudy.datasource.DataSource;
import com.db.casestudy.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/checkDB")
public class CheckDBServlet extends HttpServlet {

	private LoginService userValidationService = new LoginService();

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String dbStatus = DataSource.getInstance().checkDBConnection()
				? "You have successfully connected the Deutsche Bank server"
				: "DB NOT CONNECTED";
		ResponseUtil.setResponse(response, dbStatus);
	}


}