package com.db.casestudy.servlet;


import com.db.casestudy.ResponseUtil;
import com.db.casestudy.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

	private LoginService userValidationService = new LoginService();

    protected void doGet(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(
                request, response);

    }

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("username");
		String password =  request.getParameter("password");

		if (name != null && password != null && (userValidationService.getValidUser(name, password)) != null) {
			response.setCharacterEncoding("UTF-8");
			Cookie cookieUserName = new Cookie("user", name);
			cookieUserName.setMaxAge(30*60);
			response.addCookie(cookieUserName);

			request.getSession().setAttribute("user", name);
			response.sendRedirect(ResponseUtil.WELCOME_END_POINT);
		} else {
			request.setAttribute("errorMessage", "Invalid Credentials!");
			response.sendRedirect(ResponseUtil.LOGIN_END_POINT);
		}
	}

}