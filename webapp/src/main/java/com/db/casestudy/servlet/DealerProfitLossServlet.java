package com.db.casestudy.servlet;

import com.db.casestudy.ResponseUtil;
import com.db.casestudy.dto.DealerProfitLossDto;
import com.db.casestudy.service.RequirementService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/dealer/profitloss")
public class DealerProfitLossServlet extends HttpServlet {

    private RequirementService requirementService = new RequirementService();

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        ArrayList<DealerProfitLossDto> dealerProfitLoss = requirementService.getDealerProfitLoss();
        String profitLossJson = new ObjectMapper().writeValueAsString(dealerProfitLoss);
        ResponseUtil.setResponse(response, profitLossJson);

    }



}
