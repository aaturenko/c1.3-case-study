package com.db.casestudy.servlet;

import com.db.casestudy.ResponseUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(HttpServlet.class);

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
        Cookie loginCookie = null;
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("user")){
                    loginCookie = cookie;
                    break;
                }
            }
        }
        if(loginCookie != null){
            loginCookie.setMaxAge(0);
            response.addCookie(loginCookie);
        }

		HttpSession session = request.getSession(false);
		log.info("Log out user: "+session.getAttribute("user"));
		if(session != null){
			session.invalidate();
		}
		response.sendRedirect(ResponseUtil.LOGIN_END_POINT);

	}
}