﻿<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <title>DB Case Study C1.3</title>
  <link rel="shortcut icon" type="image/png" href="logo.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="styles.css"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>

<body>
<div class="container h-100">
    <div class="row align-items-center h-100 page-1">
        <div class="col-sm">
        </div>
        <div class="col-sm mx-auto">
            <br></br><br></br>
            <center>
                <label id="mainLabel">Case Study Group C1.3</label>
                <br></br>
                <img src="logo.png" alt="logo" height="50" width="60">
                <br></br>
            </center>
            <form id="mainForm" action="login" method="post">
                <div class="form-group">
                    <label for="inputUsername">Username</label>
                    <input type="text" name="username" id="inputUsername" class="form-control" aria-describedby="emailHelp" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" name="password" id="inputPassword" class="form-control"  placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary btn-sm" id="mainSubmit">Submit</button>
                <button type="button" class="btn btn-light btn-sm" id="checkDB" data-toggle="modal" data-target="#dbConnectionWindow">
                    Check connection
                </button>

                <div class="modal fade" id="dbConnectionWindow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Database connection</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <center>
                                    <div id="dbStatus"></div>
                                </center>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>



            </form>
        </div>
        <div class="col-sm">
        </div>
    </div>
</div>

<script>
    $('#checkDB').click(function () {
        $.ajax({
            url: 'checkDB',
            type:'GET',
            dataType:'text',
            success: function(data) {
                $('#dbStatus').html(data);
            }
        });
    });
</script>



</body>
</html>


