<%@ page import="java.util.stream.Stream" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>DB Case Study C1.3</title>
    <link rel="shortcut icon" type="image/png" href="logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="styles.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>

</head>
<body>
<%
    String userName;
    Object user = request.getSession().getAttribute("user");
    if (user != null) {
        userName = user.toString();
    }
    else {
        Cookie[] cookies = request.getCookies();
        userName = Stream.of(cookies)
                .filter(c -> c.getName().equals("user"))
                .map(Cookie::getValue)
                .findFirst()
                .orElse("User");
    }
%>
<br>
<div class="container h-100">
		<div class="row">
			<div class="col-6">
                <div style="display:inline-block">
                    <img src="smile.png" alt="logo" height="40" width="50">
                </div>
                <div style="display:inline-block">
                    <h5>Hello, <span id="userNameIntro"><%=userName%></span> !</h5>
                </div>
                <div id="underline"></div>
            </div>
            <div class="col-6 d-flex justify-content-end">
                <form action="logout" method="get">
                    <button type="submit" class="btn btn-primary btn-sm">Log out</button>
                </form>
            </div>
		</div>
		<br></br>
		<div class="row">
			<div class="col-lg">
				<div class="pos-f-t">
					<div class="collapse db-theme-color" id="navbarToggleExternalContent">
						<div class="bg-primary p-4">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">

                                            <div class="form-group">
                                                <input type="text" name="counterparty_name" id="counterpartyName" class="form-control form-control-sm" placeholder="Name">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" name="counterparty_status" id="counterpartyStatus" class="form-control form-control-sm" placeholder="Status">
                                            </div>

                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" name="instrument_name" id="instrumentName" class="form-control form-control-sm" placeholder="Instrument name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="deal_type" id="dealType" class="form-control form-control-sm" placeholder="Deal type">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">

                                            <div class="form-group">
                                                <select class="form-control form-control-sm" name id="sortField" type="text" name="sort_field" >
                                                    <option value="deal_amount">Sort by</option>
                                                    <option value="deal_time">Deal time</option>
                                                    <option value="deal_type">Deal type</option>
                                                    <option value="deal_amount">Amount</option>
                                                    <option value="deal_quantity">Quantity</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <select class="form-control form-control-sm" name id="sortType" type="text" name="sort_type" >
                                                    <option value="asc">Type of sorting</option>
                                                    <option value="asc">Ascending</option>
                                                    <option value="desc">Descending</option>
                                                </select>
                                            </div>

                                            <!--input placeholder="sort field" type="text" name="sort_field" class="form-control form-control-sm"-->

                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <button class="btn btn-outline-light btn-sm no-margin full-width" id="filterSubmit">Apply filter</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="underlineFilter"></div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <button id="realisedProfitLossButton" type="button" class="btn btn-outline-light btn-sm no-margin full-width" data-toggle="modal" data-target="#realisedProfitLossWindow">
                                            Show realised profit / loss
                                        </button>
                                        <div class="modal fade" id="realisedProfitLossWindow" tabindex="-1" role="dialog" aria-labelledby="realisedProfitLossWindow" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="realisedProfitLossTitle">Realised profit / loss</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                            <table class="table table-striped" id="realisedProfitLossTable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Realised profit / loss</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                        <button id="realisedProfitLossGraphButton" type="button" class="btn btn-outline-light btn-sm no-margin full-width" data-toggle="modal" data-target="#realisedProfitLossGraphWindow">
                                            Graph realised profit / loss
                                        </button>
                                        <div class="modal fade" id="realisedProfitLossGraphWindow" tabindex="-1" role="dialog" aria-labelledby="realisedProfitLossWindow" aria-hidden="true">
                                            <div class="modal-dialog" role="document" style="max-width: 800px  !important;">
                                                <div class="modal-content" >
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="realisedProfitLossGraphTitle">Realised profit / loss graph</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div id="realisedProfitLossGraphDiv">


                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <button id="effectiveProfitButton" type="button" class="btn btn-outline-light btn-sm no-margin full-width" data-toggle="modal" data-target="#effectiveProfitLossWindow">
                                            Show effective profit / loss
                                        </button>
                                        <div class="modal fade" id="effectiveProfitLossWindow" tabindex="-1" role="dialog" aria-labelledby="realisedProfitLossWindow" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="effectiveProfitLossTitle">Effective profit / loss</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <table class="table table-striped" id="effectiveProfitLossTable">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">Effective profit / loss</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <button id="averageBuySellButton" type="button" class="btn btn-outline-light btn-sm no-margin full-width" data-toggle="modal" data-target="#averageBuySellWindow">
                                            Show average buy / sell prices
                                        </button>
                                        <div class="modal fade" id="averageBuySellWindow" tabindex="-1" role="dialog" aria-labelledby="realisedProfitLossWindow" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="averageBuySellTitle">Average buy / sell prices</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-striped" id="averageBuySellTable">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Instrument name</th>
                                                                <th scope="col">Average buy</th>
                                                                <th scope="col">Average sell</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                        <button id="averageBuySellPricesGraphButton" type="button" class="btn btn-outline-light btn-sm no-margin full-width" data-toggle="modal" data-target="#averageBuySellPricesGraphWindow">
                                            Graph average buy / sell prices
                                        </button>
                                        <div class="modal fade" id="averageBuySellPricesGraphWindow" tabindex="-1" role="dialog" aria-labelledby="averageBuySellPricesGraphWindow" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="averageBuySellPricesGraphTitle">Average buy / sell prices graph</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                            <div class="form-group">
                                                                <form>
                                                                    <label for="averageBuySellPricesGraphSelect">Please select an option</label>
                                                                    <select class="form-control" id="averageBuySellPricesGraphSelect">
                                                                        <option>Show average buy prices</option>
                                                                        <option>Show average sell prices</option>
                                                                    </select>
                                                                </form>
                                                            </div>
                                                        <div id="averageBuySellPricesGraphDiv" style="height: 360px;">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <button id="netTradeButton" type="button" class="btn btn-outline-light btn-sm no-margin full-width" data-toggle="modal" data-target="#netTradeWindow">
                                            Show net trades
                                        </button>
                                        <div class="modal fade" id="netTradeWindow" tabindex="-1" role="dialog" aria-labelledby="netTradeWindow" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="netTradeTitle">Net trades</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <form>
                                                                <label for="counterPartyNameNetTrade">Please select name</label>
                                                                <select class="form-control" id="counterPartyNameNetTrade">
                                                                    <option>Please select</option>
                                                                </select>
                                                            </form>
                                                        </div>

                                                        <table class="table table-striped" id="netTradeTable">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Instrument name</th>
                                                                <th scope="col">Net trade</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
					<nav class="navbar navbar-dark bg-primary">
						<button class="navbar-toggler btn-primary" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span> Trading data
						</button>
					</nav>
				</div>
			</div>
		</div>
		<br></br>
		<div class="row">
			<div class="col-lg">
				<table class="table table-striped" id="mainTable">
					<thead>
					<tr>
						<th scope="col">Deal Time</th>
						<th scope="col">Name</th>
						<th scope="col">Status</th>
						<th scope="col">Instrument Name</th>
						<th scope="col">Deal Type</th>
						<th scope="col">Amount</th>
						<th scope="col">Quantity</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<br></br>
		<div class="row">
			<div class="col-lg">
				<nav aria-label="...">
					<ul class="pagination mx-auto pagination-width">
						<li class="page-item" id="prevLink">
							<a class="page-link page-link-previous" tabindex="-1" >Previous</a>
						</li>
						<li class="page-item" id="nextLink">
							<a class="page-link page-link-next" >Next</a>
						</li>
					</ul>
				</nav>
                <br></br>
			</div>
		</div>
	</div>

<script>
    var globalPage;
    var globalDataNetTrade = "";

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);

    }

$(document).ready(function () {
    var url = 'deal';

    var userNameIntro = $('#userNameIntro').text();

    globalPage = 0;
    $('#prevLink').addClass('disabled');

    $('#userNameIntro').text(capitalizeFirstLetter(userNameIntro));
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function(data) {
            $.each(data, function(index, value) {
            $('#mainTable > tbody:last').append('<tr><td>' + new Date(Date.parse(value["dealTime"])).toDateString("yyyy-MM-dd") + '</td>' +
                '<td>' + value["counterparty"]["name"] + '</td>' +
                '<td>' + value["counterparty"]["status"] + '</td>' +
                '<td>' + value["instrument"]["instrumentName"] + '</td>' +
                '<td>' + value["dealType"] + '</td>' +
                '<td>' + value["amount"] + '</td>' +
                '<td>' + value["quantity"] +
                '</td></tr>');
            });
    }
});


    });

$("#realisedProfitLossButton").click(
  function () {
        $('#realisedProfitLossTable > tbody').html('<tr></tr>');
        $.ajax({
            type: "POST",
            url: "dealer/profitloss",
            success: function (data) {
                $.each(data, function(index, value) {
                    $('#realisedProfitLossTable > tbody:last').append('<tr><td>' + value["counterpartyName"] + '</td>' +
                        '<td>' + value["realisedProfit"] + '</td></tr>');
                });
            }
        });
  });

$("#effectiveProfitButton").click(
    function () {
        $('#effectiveProfitLossTable > tbody').html('<tr></tr>');
        $.ajax({
            type: "POST",
            url: "dealer/effectiveprofit",
            success: function (data) {
                $.each(data, function(index, value) {
                    $('#effectiveProfitLossTable > tbody:last').append('<tr><td>' + value["instrumentName"] + '</td>' +
                        '<td>' + value["effectiveProfit"] + '</td></tr>');
                });
            }
        });
    });

$("#averageBuySellButton").click(
    function () {
        $('#averageBuySellTable > tbody').html('<tr></tr>');
        $.ajax({
            type: "POST",
            url: "instrument/average",
            success: function (data) {
                $.each(data, function(index, value) {
                    $('#averageBuySellTable > tbody:last').append('<tr><td>' + value["instrument_name"] + '</td>' +
                        '<td>' + value["averageBuy"] + '</td>' +
                        '<td>' + value["averageSell"] + '</td>' +
                        '</tr>');
                });
            }
        });
    });

$("#netTradeButton").click(
    function () {
        $('#netTradeTable > tbody').html('<tr></tr>');
        $('#counterPartyNameNetTrade').html('<option>Please select</option>');
        $.ajax({
            type: "POST",
            url: "dealer/nettrade",
            success: function (data) {
                globalDataNetTrade = data;

                $.each(data, function(index, value) {
                    $('#counterPartyNameNetTrade:last').append('<option>' + value["counterpartyName"] + '</option>');
                });

                $.each(data[0]["netTrades"], function(index, value) {
                    $('#netTradeTable > tbody:last').append('<tr><td>' + value["instrumentName"] + '</td>' +
                        '<td>' + value["netTrade"] + '</td>' +
                        '</tr>');
                });
            }
        });
    });

$('#counterPartyNameNetTrade').change(function(){
    $.each(globalDataNetTrade, function(index, value) {
        if ($('#counterPartyNameNetTrade').val() == value["counterpartyName"] ) {
            $('#netTradeTable > tbody').html('<tr></tr>');
            $.each(value["netTrades"], function(index2, value2) {
                $('#netTradeTable > tbody:last').append('<tr><td>' + value2["instrumentName"] + '</td>' +
                    '<td>' + value2["netTrade"] + '</td>' +
                    '</tr>');
            });
        }
    });
});

var readFilerDataAndUpdateTable = function () {

    var name = $('#counterpartyName').val();
    var status = $('#counterpartyStatus').val();
    var instrument = $('#instrumentName').val();
    var dealType = $('#dealType').val();
    var sortField = $('#sortField').val();
    var sortType = $('#sortType').val();

    $.ajax({
        type:'POST',
        url:"deal",
        dataType: "json",
        data: {
            page: globalPage,
            counterparty_name: name,
            counterparty_status: status,
            instrument_name: instrument,
            deal_type: dealType,
            sort_field: sortField,
            order: sortType
        },
        success: function (data) {
            $('#mainTable > tbody').html('<tr></tr>');
            $.each(data, function(index, value) {
                $('#mainTable > tbody:last').append('<tr><td>' + new Date(Date.parse(value["dealTime"])).toDateString("yyyy-MM-dd") + '</td>' +
                    '<td>' + value["counterparty"]["name"] + '</td>' +
                    '<td>' + value["counterparty"]["status"] + '</td>' +
                    '<td>' + value["instrument"]["instrumentName"] + '</td>' +
                    '<td>' + value["dealType"] + '</td>' +
                    '<td>' + value["amount"] + '</td>' +
                    '<td>' + value["quantity"] +
                    '</td></tr>');
            });
        }
    });
}

$('#nextLink > a').click(function () {
    if (globalPage == 0) {
        $('#prevLink').removeClass('disabled');
    }
    globalPage++;
    if (globalPage == 100) {
        $('#nextLink').addClass('disabled');
    }

    readFilerDataAndUpdateTable();

});



$('#prevLink > a').click(function () {
    if (globalPage == 100) {
        $('#nextLink').removeClass('disabled');
    }
    globalPage--;
    if (globalPage == 0) {
        $('#prevLink').addClass('disabled');
    }

    readFilerDataAndUpdateTable();

});

$('#filterSubmit').click(function () {

    globalPage = 0;
    $('#prevLink').addClass('disabled')
    readFilerDataAndUpdateTable();

});

</script>

<script src="d3.v3.min.js" charset="utf-8"></script>
<script>

    $("#averageBuySellPricesGraphButton").click(function () {
        $.ajax({
            type: "POST",
            url: "instrument/average",
            success: function (data) {

                var averageBuySellPrices = data;
                var globalDomain = [];
                var globalRange = [];
                var globalBuyPricesValues = [];
                var globalSellPricesValues = [];
                var globalBuyData = [];
                var globalSellData = [];
                var globalColorsBuy = ["#20c3ff", "#86cece", "#4553c1", "#f5d6fd"];
                var globalColorsSell = ["#86cece", "#20c3ff", "#f5d6fd", "#4553c1"];


                $.each(averageBuySellPrices, function(index, value) {
                    globalDomain.push(value["instrument_name"]);
                    globalBuyPricesValues.push(value["averageBuy"]);
                    globalSellPricesValues.push(value["averageSell"]);
                    globalBuyData.push({label:value["instrument_name"],value:value["averageBuy"]});
                    globalSellData.push({label:value["instrument_name"],value:value["averageSell"]});
                });
                $("#averageBuySellPricesGraphDiv").html("");
                var svg = d3.select("#averageBuySellPricesGraphDiv")
                    .append("svg")
                    .append("g");
                svg.append("g")
                    .attr("class", "slices");
                svg.append("g")
                    .attr("class", "labels");
                svg.append("g")
                    .attr("class", "lines");

                var width = 350, height = 300,
                    radius = Math.min(width, height) / 2;

                var pie = d3.layout.pie()
                            .sort(null)
                            .value(function(d) {
                                        return d.value;
                                    });

                var arc = d3.svg.arc()
                                .outerRadius(radius * 0.6)
                                .innerRadius(radius * 0.2);

                var outerArc = d3.svg.arc()
                                    .innerRadius(radius * 0.9)
                                    .outerRadius(radius * 0.9);

                svg.attr("transform", "translate(" + width / 1.6 + "," + height / 2 + ")");

                var key = function(d){ return d.data.label; };

                var color = d3.scale.ordinal()
                            .domain(globalDomain)
                            .range(["#20c3ff", "#86cece", "#4553c1", "#f5d6fd"]);

                function returnBuyData (){
                    return globalBuyData;
                }

                function returnSellData (){
                    return globalSellData;
                }

                change(returnBuyData());

                d3.select("#averageBuySellPricesGraphSelect")
                    .on("change", function(){
                        switch($("#averageBuySellPricesGraphSelect").val())
                        {
                            case "Show average buy prices":
                                change(returnBuyData());
                                break;
                            case "Show average sell prices":
                                change(returnSellData());
                                break;
                        }
                    });

                function change(data) {
                    var slice = svg.select(".slices").selectAll("path.slice")
                        .data(pie(data), key);

                    slice.enter()
                        .insert("path")
                        .style("fill", function(d) { return color(d.data.label); })
                        .attr("class", "slice");

                    slice
                        .transition().duration(1000)
                        .attrTween("d", function(d) {
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                return arc(interpolate(t));
                            };
                        })

                    slice.exit()
                        .remove();

                    var text = svg.select(".labels").selectAll("text")
                        .data(pie(data), key);

                    text.enter()
                        .append("text")
                        .attr("dy", ".35em")
                        .text(function(d) {
                            return d.data.label;
                        });

                    function midAngle(d){
                        return d.startAngle + (d.endAngle - d.startAngle)/2;
                    }

                    text.transition().duration(1000)
                        .attrTween("transform", function(d) {
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                var d2 = interpolate(t);
                                var pos = outerArc.centroid(d2);
                                pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                                return "translate("+ pos +")";
                            };
                        })
                        .styleTween("text-anchor", function(d){
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                var d2 = interpolate(t);
                                return midAngle(d2) < Math.PI ? "start":"end";
                            };
                        });

                    text.exit()
                        .remove();

                    var polyline = svg.select(".lines").selectAll("polyline")
                        .data(pie(data), key);

                    polyline.enter()
                        .append("polyline");

                    polyline.transition().duration(1000)
                        .attrTween("points", function(d){
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                var d2 = interpolate(t);
                                var pos = outerArc.centroid(d2);
                                pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                                return [arc.centroid(d2), outerArc.centroid(d2), pos];
                            };
                        });

                    polyline.exit()
                        .remove();
                };
                }
        });
    })
</script>


<script>

    $("#realisedProfitLossGraphButton").click(function () {
        $("#realisedProfitLossGraphDiv").html('');
        $.ajax({
            type: "POST",
            url: "dealer/profitloss",
            dataType: "json",
            success: function (data) {
                var margin = {top: 1, right: 1, bottom: 1, left: 1},
                    width = 800 - margin.left - margin.right,
                    height = 600 - margin.top - margin.bottom;

                var x = d3.scale.linear()
                    .range([0, width]);

                var y = d3.scale.ordinal()
                    .rangeRoundBands([0, height], 0.1);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickSize(0)
                    .tickPadding(6);

                var svg = d3.select("#realisedProfitLossGraphDiv").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                x.domain(d3.extent(data, function(d) { return d.realisedProfit; })).nice();
                y.domain(data.map(function(d) { return d.counterpartyName; }));

                svg.selectAll(".bar")
                    .data(data)
                    .enter().append("rect")
                    .attr("class", function(d) { return "bar bar-" + (d.realisedProfit < 0 ? "negative" : "positive"); })
                    .attr("x", function(d) { return x(Math.min(0, d.realisedProfit)); })
                    .attr("y", function(d) { return y(d.counterpartyName); })
                    .attr("width", function(d) { return Math.abs(x(d.realisedProfit) - x(0)); })
                    .attr("height", y.rangeBand());

                svg.append("g")
                    .attr("class", "y axis")
                    .attr("transform", "translate(" + x(0) + ",0)")
                    .call(yAxis);


                function type(d) {
                    d.realisedProfit = +d.realisedProfit;
                    return d;
                }

            }
        });
    });

</script>


</body>
</html>